console.log('Cargando tarjetas')
const dataCards = [

{
    "title": "Diseños para la Universidad UCEM",
    "url_image":"https://scontent.fsjo7-1.fna.fbcdn.net/v/t1.6435-9/162606426_2976390509349792_460569833195019568_n.png?_nc_cat=109&ccb=1-3&_nc_sid=e3f864&_nc_ohc=NscEW9koAOoAX8lemW_&_nc_ht=scontent.fsjo7-1.fna&oh=7df74dc0c161d55644f59605a373ee89&oe=6090E5A2",
    "desc":"Anuncios que hago a las redes sociales de la universidad UCEM.",
    "cta":"Ver galeria",
    "link":"https://www.facebook.com/UCEMOficial/photos/?ref=page_internal"
},
    
{
    "title": "Diseños para la empresa Tigo",
    "url_image":"https://scontent.fsjo7-1.fna.fbcdn.net/v/t1.6435-9/165204816_794284927866906_193198055219872214_n.png?_nc_cat=106&ccb=1-3&_nc_sid=e3f864&_nc_ohc=znmeZtcIIHYAX-jKh4h&_nc_ht=scontent.fsjo7-1.fna&oh=451cf1ce985f51bd1ac2a6473cb487fc&oe=60915059",
    "desc":"Anuncios que elaboro para ayudar a un vendedor de cable e internet de la empresa Tigo",
    "cta":"Ver galeria",
    "link":"https://www.facebook.com/Asesor-de-ventas-Ren%C3%A9-Rodr%C3%ADguez-Tigo-CR-629105364384864/photos/?ref=page_internal"
},

{
    "title": "Diseños para una tienda de antigüedades La historia en dinero R.S",
    "url_image":"https://scontent.fsjo7-1.fna.fbcdn.net/v/t1.6435-9/101250314_2371761953129998_5980507501710278656_n.png?_nc_cat=110&ccb=1-3&_nc_sid=e3f864&_nc_ohc=SWeWR-FsbocAX8T08uC&_nc_ht=scontent.fsjo7-1.fna&oh=ecf75e94ae89f44989d9900e7184d379&oe=6093AE28",
    "desc":"Varios diseños que hice a esta tienda para coleccionistas de objetos históricos, con estética antigua y rústica.",
    "cta":"Ver galería",
    "link":"https://www.facebook.com/LahistoriaendineroR.S/photos/?ref=page_internal"
},

{
    "title": "Diseños para una tienda de accesorios llamada Yvendo",
    "url_image":"https://scontent.fsjo7-1.fna.fbcdn.net/v/t1.6435-9/98845148_101933431536318_2428852234075766784_n.png?_nc_cat=101&ccb=1-3&_nc_sid=e3f864&_nc_ohc=CLPfCiOqTscAX9w_Fbb&_nc_ht=scontent.fsjo7-1.fna&oh=8b905ee28ffb64dffcf96ae176089215&oe=609110B2",
    "desc":"Imágenes que monto para poder publicitar esta tienda de accesorios y ropa que se desarrolla en Facebook.",
    "cta":"Ver galería",
    "link":"https://www.facebook.com/YvendoCR/photos/?ref=page_internal"
},

{
    "title": "Diseños a una pequeña empresa Bread Freezer",
    "url_image":"https://scontent.fsjo7-1.fna.fbcdn.net/v/t1.6435-9/162977030_1783349541844048_7919724193028112793_n.png?_nc_cat=101&ccb=1-3&_nc_sid=e3f864&_nc_ohc=l79K4-mdWHUAX_sTAS9&_nc_ht=scontent.fsjo7-1.fna&oh=54037a08402b588820aadf34b8a3db89&oe=6090B3B7",
    "desc":"Algunos anuncios que hice a esta empresa de panes congelados para renovar un poco su publicidad.",
    "cta":"Ver galería",
    "link":"https://www.facebook.com/breadfreezer"
},

{
    "title": "También se realizan pinturas y dibujos para cualquier ocasión",
    "url_image":"https://scontent.fsjo7-1.fna.fbcdn.net/v/t1.6435-9/71765476_100203168061043_9127712287560302592_n.jpg?_nc_cat=109&ccb=1-3&_nc_sid=e3f864&_nc_ohc=EPc8UjIHPYUAX8RZIqI&_nc_ht=scontent.fsjo7-1.fna&oh=1bd72d4752717f5e129ec23369e31cc8&oe=609446F8",
    "desc":"Además de realizar diseños publicitarios también tengo un apartado para poder mostrar mis obras y vender cuadros.",
    "cta":"Ver galería",
    "link":"https://www.facebook.com/leonartemunoz/photos/?ref=page_internal"
},

];

(function(){
    let CARD = {
        init: function (){
            console.log('card module was loaded');
            let _self = this;

            //Llamamos las funciones
            this.insertData(_self);
            //this.eventHandler(_self);
        },

        eventHandler: function (_self) {
            let arrayRefs = document.querySelectorAll('.accordion-title');

            for (let x = 0; x < arrayRefs.length; x++) {
                arrayRefs[x].addEventListener('click', function(event){
                    console.log('event', event);
                    _self.showTab(event.target);
                });
            }
        },

        insertData: function (_self) {
            dataCards.map(function (item, index){
                document.querySelector('.card-list').insertAdjacentHTML('beforeend', _self.tplCardItem(item, index));
            });
    },

    tplCardItem: function (item, index) {
        return(`<div class='card-item' id="card-number-${index}">
        <img src="${item.url_image}"/>
        <div class="card-info">
        <p class='card-title'>${item.title}</p>
        <p class='card-desc'>${item.desc}</p>
        <a class='card-cta' target="blank" href="${item.link}">${item.cta}</a>
        </div>
        </div>`)},
    }

    CARD.init();
})();