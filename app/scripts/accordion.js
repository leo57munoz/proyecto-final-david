const dataAccordion = [{
    "title": "¿Por qué es bueno mejorar la imagen de su empresa?",
  "desc": "Todo entra por la vista, y si su empresa no cuenta con una imagen llamativa o atrayente no llegará tanta gente."
},
{
    "title": "¿Cómo se le puede dar un mejor aspecto a la publicidad de su empresa?",
    "desc": "Con diseños más modernos, elaborados y sofisticados para que la gente se sienta interesada en el producto que usted vende."
  },
  {
    "title": "¿Es buena idea pagar por anuncios más atractivos para atraer clientes?",
    "desc": "Es una inversión muy buena que le hará crecer su clientela tanto en internet como fuera de este."
  },
  {
    "title": "¿Por qué su empresa no está creciendo tanto como otras?",
    "desc": "Quizás se deba a que su publicidad o la manera en que se dan a conocer no es tan buena tomando en cuenta la estética en los diseños que usan."

}];

(function (){
    let ACCORDION = {
        init: function (){
            let _self = this;
            //Se llaman las funciones
            this.insertData(_self);
            this.eventHandler(_self);
        },

        eventHandler:function(_self){
            let arrayRefs = document.querySelectorAll('.accordion-title');

            for (let x = 0; x < arrayRefs.length; x++) {
                arrayRefs[x].addEventListener('click', function(event){
                    console.log('event', event);
                    _self.showTab(event.target);
                });
            }
        },

        showTab: function(refItem){
            let activeTab = document.querySelector('.tab-active');

            if(activeTab){
                activeTab.classList.remove('tab-active');
            }

            console.log('show tab', refItem);
            refItem.parentElement.classList.toggle('tab-active');
        },

        insertData: function (_self) {
            dataAccordion.map(function (item, index){
                //console.log('item!!!!!', item);
                document.querySelector('.main-accordion-container').insertAdjacentHTML('beforeend', _self.tplAccordionItem(item));
            });
        },

        tplAccordionItem: function (item) {
            return(` <div class='accordion-item'>
            <p class='accordion-title'>${item.title}</p>
            <p class='accordion-desc'>${item.desc}</p>
            </div>`)},
        }

        ACCORDION.init();

})();